#include <iostream>
#include <list>
using namespace std;

int main()
{
	int round = 0; //counts round
	list <int> playerList;
	list<int>::iterator check, display; //one for check, anotherone for display

	int playerQuantity, pass;

	cout << "How many players? : ";
	cin >> playerQuantity;

	cout << "How many passes? : ";
	cin >> pass;

	for (int i = 1; i <= playerQuantity; i++) {
		playerList.push_back(i); //create a list
	}
	check = playerList.begin(); //set check at first person
	
	//prints out before game start
	cout << "Round : " << round << endl;
	for (display = playerList.begin(); display != playerList.end(); ++display) {
		cout << ' ' << *display;
	}
	cout << endl;
	

	while (playerList.size() >1) { //stop when the player == 1
		round++;
		
		for (int i = 1; i <= pass; i++) {
			if (check == playerList.end()) {
				check = playerList.begin(); //jump to first and +1
				check++;
			}
			else {
				check++; //pass to next one
			}
		}

		if (check == playerList.end()) {
			check = playerList.begin(); //when check runs to last then jump to first one again
		}


		cout << "----------------" << endl;
		cout << "Round : " << round << endl;
		cout << *check << " has eliminated." << endl;

		check = playerList.erase(check); //eliminate
		

		for (display = playerList.begin(); display != playerList.end(); ++display){
			cout << ' ' << *display;
		}
		cout << endl;
		
	}
}